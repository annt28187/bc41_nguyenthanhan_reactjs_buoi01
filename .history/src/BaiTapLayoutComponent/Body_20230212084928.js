import React, { Component } from 'react';
import Banner from './Banner';

export default class Body extends Component {
  render() {
    return (
      <div>
        <Banner />
      </div>
    );
  }
}
