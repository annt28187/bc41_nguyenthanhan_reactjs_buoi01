import './App.css';
import Layout from './BaiTapLayoutComponent/Layout';

function App() {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
